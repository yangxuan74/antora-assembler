= Antora Assembler
Dan Allen; Sarah White
:url-repo: https://gitlab.com/antora/antora-assembler
:url-rvm: https://rvm.io
:url-pdf-docs: https://docs.asciidoctor.org/pdf-converter/latest
:url-antora-docs: https://docs.antora.org/antora/latest

The purpose of the Antora Assembler project is to provide a means of aggregating and exporting content in an Antora site to PDF and other formats in such a way that all references within the Antora coordinate system are preserved.

WARNING: This is alpha software!
That means this software is experimental and likely to change at any time without notice.
You're free to test it and give feedback in the project chat, but take caution when relying on it in a production site.

== Overview

This project manages three packages:

Antora PDF Extension (@antora/pdf-extension)::
The Antora PDF Extension is the official extension for producing PDFs from content in an Antora site.
It generates PDFs of aggregate pages and publishes them alongside other files in the site.
The extension calls Assembler to construct aggregate AsciiDoc documents based on the navigation tree per component version.
It then iterates over those aggregate documents and converts them to PDF using Asciidoctor PDF or the specified command.
Finally, it pushes those PDFs into the site catalog and Antora publishes them alongside the other files in the site.

Assembler (@antora/assembler)::
Assembler is the core library for writing Antora extensions that combine content from multiple pages into a single output file.
Assembler works by constructing aggregate, self-contained AsciiDoc documents from the pages listed in the navigation tree per component version.
These aggregate documents are Antora-agnostic, which means they can be converted by any AsciiDoc converter, such as Asciidoctor PDF.
Assembler then invokes the specified callback to convert these documents to an output format such as PDF.
If a catalog is specified, Assembler pushes the converted files back into the catalog to be published alongside the other files in the site.
+
Assembler is installed automatically when you install the Antora PDF Extension, which is currently the main entry point for Assembler.

Assembler Test Harness (private)::
This package is only used to support the test suite and therefore isn't released and published to npm.
The test harness provides the infrastructure for writing and running tests for Assembler and extensions based on it.

== Prerequisites

The Antora PDF Extension requires Ruby 2.7 or greater, Asciidoctor PDF 2, Node.js 16, and Antora 3.
We assume you've already set up a playbook project, an Antora playbook file (i.e., [.path]_antora-playbook.yml_), and that you're executing the commands shown in the following sections from your playbook project.

=== Antora Playbook Project

We highly recommend that you install this software and its prerequisites in your {url-antora-docs}/playbook/use-an-existing-playbook-project/[playbook project^], the directory where your site's Antora playbook file (e.g., [.path]_antora-playbook.yml_) is located.
If you install the prerequisites and PDF extension globally, conflicts with other installed software and versions may occur.

When you're evaluating Antora Assembler for the first time, we recommend starting with the Antora demo.

 $ git clone https://gitlab.com/antora/demo/docs-site.git assembler-tutorial && cd assembler-tutorial

By starting out this way, you can learn how Antora Assembler without having to worry about other complexities.
We know that Antora Assembler works with the Antora Demo.
Once you get that working, you can move on to using it on your own Antora playbook project while still being able to refer back to the demo.

=== Ruby

Asciidoctor PDF 2 requires either Ruby 2.7 or greater or JRuby 9.2 or greater.
We strongly encourage you to use the latest stable Ruby version.
To check if you have Ruby available, run the `ruby` command to print the installed version:

 $ ruby -v

You should see a version string, such as:

 $ ruby -v
 ruby 3.1.0p0 (2021-12-25 revision fb4df44d16) [x86_64-linux]

Make sure this command reports a Ruby version that starts with 2.7 or greater (or a JRuby version that starts with 9.2 or greater).
If it doesn't, go to {url-rvm}[rvm.io^] to set up RVM and use it to install a version of Ruby.

[#install-asciidoctor-pdf]
=== Asciidoctor PDF

The Antora PDF Extension uses {url-pdf-docs}/[Asciidoctor PDF 2^] to generate PDF files.
We recommend {url-pdf-docs}/install/#install-asciidoctor-pdf[installing Asciidoctor PDF^] into your project using a [.path]_Gemfile_ managed by Bundler.
Start by creating a file named [.path]_Gemfile_ and adding an entry for `asciidoctor-pdf` as follows:

.Gemfile
[,ruby]
----
source 'https://rubygems.org'

gem 'asciidoctor-pdf'
----

First, configure Bundler to install gems inside the project:

 $ bundle config --local path .bundle/gems

Then run Bundler as follows:

 $ bundle

If you want to apply syntax highlighting to source blocks in your PDF files, you'll also need a syntax highlighter that is supported by Asciidoctor PDF.
Let's install Rouge by adding an entry for the `rouge` gem in the [.path]_Gemfile_.

.Gemfile
[,ruby]
----
source 'https://rubygems.org'

gem 'asciidoctor-pdf'
gem 'rouge'
----

Run the `bundle` command again.

If you prefer to install Asciidoctor PDF globally, you can use the `gem install` command instead.
Pass the name of the gem, *asciidoctor-pdf*, to the `gem install` command as follows:

 $ gem install asciidoctor-pdf

If you have problems installing Asciidoctor PDF, see the installation {url-pdf-docs}/install/#troubleshooting[troubleshooting tips^].

=== Node.js

This software requires Node.js 16.
To see if you have Node.js installed, and which version, type:

 $ node -v

You should see a version string, such as:

 $ node -v
 v16.15.0

If no version number is displayed in your terminal, you need to install Node.js.
Follow one of these guides to learn how to install nvm and Node.js on your platform.

* {url-antora-docs}/install/linux-requirements/#install-nvm[Install nvm and Node.js on Linux]
* {url-antora-docs}/install/macos-requirements/#install-nvm[Install nvm and Node.js on macOS]
* {url-antora-docs}/install/windows-requirements/#install-choco[Install nvm and Node.js on Windows]

=== Antora

Assembler and the PDF extension require Antora 3.
To confirm you have Antora 3 installed in your playbook directory, run:

 $ npx antora -v

The command should report the version of the Antora CLI and site generator you have installed.

 $ npx antora -v
 @antora/cli: 3.0.1
 @antora/site-generator: 3.0.1

If you're not yet using Antora 3, you need to {url-antora-docs}/upgrade-antora/#upgrade-antora-locally[upgrade^] to use the PDF extension.

[#install-pdf-extension]
== Install the Antora PDF Extension

Once you've satisfied the prerequisites, you're ready to install the Antora PDF Extension in your playbook project.
To install the Antora PDF Extension package in your playbook project, type the following command:

 $ npm i @antora/pdf-extension

This command installs the PDF extension and its dependencies, including the supporting Assembler library.

== Registration

Once the Antora PDF Extension is installed, you need to {url-antora-docs}/extend/register-extension/[register the extension^] with Antora.
To register the extension, add an entry in your {url-antora-docs}/playbook/[playbook file^] that specifies the name of the package under the `antora.extensions` key.
Open your Antora playbook file and add the extension as follows:

.antora-playbook.yml
[,yaml]
----
antora:
  extensions:
  - '@antora/pdf-extension'
# ...
----

NOTE: The quotes are required around the package name because `@` is a special character in YAML.

Alternately, you can register the PDF extension at your discretion using the `--extension` CLI option of the `antora` command:

 $ antora --extension @antora/pdf-extension antora-playbook.yml

== Configuration

The PDF extension builds PDF files from the content defined in the playbook file, organizing it based on the structure of the navigation.
You can further configure the behavior of the PDF extension in the [.path]_antora-assembler.yml_ file adjacent to your Antora playbook file.
This YAML file defines the PDF configuration keys and AsciiDoc attributes that are only applied to the PDFs.

The AsciiDoc attributes defined in [.path]_antora-assembler.yml_ override any matching attributes in the playbook file, component descriptor files, or pages according to Antora's {url-antora-docs}/playbook/asciidoc-attributes/#precedence-rules[attribute precedence rules^].

Let's set up a basic [.path]_antora-assembler.yml_ file.

. Open a new file in the text editor or IDE of your choice, name it [.path]_antora-assembler.yml_, and save it adjacent to your Antora playbook file (i.e., [.path]_antora-playbook.yml_).
. Let's create one PDF per {url-antora-docs}/component-version/[component version^].
In the new file, type the key name `root_level` directly followed by a colon (`:`) and a space.
Then enter the value `0` (without enclosing it in quotes).
+
--
.antora-assembler.yml
[,yaml]
----
root_level: 0
----

The value `0` makes one PDF per component version.

TIP: If you want to make one PDF per top-level entry in the navigation tree of each component version, use the value `1` instead.
--

. By default, the PDF extension generates PDFs for all the component versions in your site.
You can specify individual or a range of component versions using the `component_versions` key.
To only generate PDFs for the latest version of each component, assign the value `@*` to the key.
+
--
.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
----

The `component_versions` key accepts picomatch pattern values so you can filter what PDFs are generated by version and component name.
--

. Now, let's set some AsciiDoc attributes for the PDFs.
Attributes are mapped under the `asciidoc` and `attributes` keys in the configuration file.
These attributes will be applied to all the generated PDFs according to the {url-antora-docs}/playbook/asciidoc-attributes/#precedence-rules[attribute precedence rules^].
+
.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    attribute-name-here: attribute-value-here
----

. Let's set a built-in attribute named `source-highlighter` and assign it the value `rouge`.
This will allow non-ASCII characters to be used in the title of the PDF.
Enter the name of the attribute, followed by a colon (`:`), a space, and the value of the attribute.
+
.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    source-highlighter: rouge
----

. Let's now set up an Asciidoctor PDF theme for customizing the PDFs the exporter generates.
Start by creating a theme file in your playbook directory named [.path]_pdf-theme.yml_.
To keep it simple, this theme extends the default theme and sets the font color on a role named `red`.
This gives you something to build on later.
+
.pdf-theme.yml
[,yaml]
----
extends: default
role:
  red:
    font-color: #FF0000
----

. In your [.path]_antora-assembler.yml_ file, activate the PDF theme by specifying the built-in `pdf-theme` attribute.
The `pdf-theme` attribute accepts the name of a YAML file stored in your playbook directory.
In this example, the file is named [.path]_pdf-theme.yml_.
+
--
.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
----

You can customize your PDF theme using {url-pdf-docs}/theme/[Asciidoctor PDF's theming language^].
--
. Finally, if you're using Bundler to manage the Asciidoctor PDF gem, you'll need to specify the command using the `build.command` key so that Assembler runs Asciidoctor PDF through `bundle exec`.
The default command is `asciidoctor-pdf`, which will only work if you have installed Asciidoctor PDF globally.
+
.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
----
+
[TIP]
====
Specifying the command key gives you an opportunity to pass CLI options to the command.
If you want Asciidoctor PDF to print the full backtrace of an error for debugging purposes, add the `--trace` option.
If you want Asciidoctor PDF to report the file and line number for warnings (when available), add the `--sourcemap` option (Asciidoctor PDF >= 2.2.0).

.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf --trace --sourcemap
----

You can also use this capability to load Asciidoctor extensions for Asciidoctor PDF, such as Asciidoctor Kroki.
First, make sure the gem is declared in [.path]_Gemfile_.
Then, instruct Asciidoctor PDF to require the gem by passing the gem name to the `-r` option.

.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    allow-uri-read: ''
    kroki-fetch-diagram: ~
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf -r asciidoctor-kroki
----

Notice we've set both the `allow-uri-read` and `kroki-fetch-diagram` AsciiDoc attributes.
These settings are required when using Kroki with Antora Assembler.

Run `bundle exec asciidoctor-pdf -h` to get a list of available options.
Note that some options are not applicable in this environment (such as `-D`).
====

You're almost there!
Here's the entire PDF configuration file you've assembled so far.

.antora-assembler.yml
[,yaml]
----
root_level: 0
component_versions: '*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
build:
  command: bundle exec asciidoctor-pdf
----

TIP: If you want to make one PDF per top-level entry in the navigation of each component version, set the `root_level` key to the value `1` instead.

This PDF configuration file will generate a PDF for the latest version of each of your components using the repository branches specified in your playbook file.
All you have to do before running Antora is save the updates you've made to this file.
Remember, the file must be named [.path]_antora-assembler.yml_ and it must be in your Antora playbook directory (i.e., adjacent to [.path]_antora-playbook.yml_).

Once you've saved the configuration file, you're ready to run Antora and export your PDFs.

== Generate PDFs

To generate your site and the corresponding PDFs, run Antora.

 $ npx antora antora-playbook.yml

Antora uses the playbook file to generate your site and your PDFs.
Notice that you don't specify the name of the Assembler configuration file; Antora will locate that file automatically.

Once you run Antora, each PDF will be written to the build directory.
The build directory is where the artifacts for the PDF files are assembled and the PDF files are generated.
By default, the directory location is [.path]_./build/assembler_.
This location can be customized by assigning a new value to the `build.dir` key in the [.path]_antora-assembler.yml_ file.
The PDFs are also published to your site.

== Publishing

The PDF extension always generates PDFs into the build directory (`build.dir`) specified in [.path]_antora-assembler.yml_ (default: [.path]_./build/assembler_).
By default, the PDF extension will also publish the PDFs alongside the HTML files in the site.

If you don't want the PDF extension to publish the PDFs with the site, set the following key in your [.path]_antora-assembler.yml_ file:

.antora-assembler.yml
[,yaml]
----
build:
  publish: false
----

If you don't want Antora to publish the site (i.e., the HTML files), set the following key in your Antora playbook file:

.antora-playbook.yml
[,yaml]
----
output:
  destinations: []
----

You'll still find the PDFs generated by the PDF extension under the build directory.

== PDF Configuration Keys Reference

The table below lists (some of) the keys that can be defined in [.path]_antora-assembler.yml_.

[cols="2,6,2,2"]
|===
|Name |Description |Default |Values

|`component_versions`
a|A filter that specifies which component versions the generator exports to PDFs.
The value(s) can be one or more picomatch patterns.

* `*` matches only the latest version of all components.
* `**` matches all components and versions.
* `<component-name>` matches the latest version of the specified component.
* `{<component-name-1>,<component-name-2>}` matches the latest version of two different components.
* `*@<component-name>` matches all versions of the specified component.
* ...and so on

|`*`
|Array of picomatch patterns

|`root_level`
a|The navigation list entry level from which to start making PDFs (`0` or `1`).

* `0` makes one PDF per component version.
* `1` makes a PDF per top-level entry in the navigation tree of each component version.

|`0`
|`0` or `1`

|`section_merge_strategy`
a|Controls how the generator merges sections from the pages with sections created to represent navigation entries.

* `discrete` converts all section titles in the page to discrete headings (which will not appear in PDF TOC or outline).
* `fuse` inserts sections from the page as children of the nav entry for the current page.
The top-level section titles in the page are inserted before the existing children of the nav entry, effectively becoming siblings.
* `enclose` inserts a new child entry into the nav for the current page and inserts the sections from the page as children of that entry.
The title of this new nav entry is controlled by the `overview-title` AsciiDoc attribute, which defaults to "`Overview`".
The new nav entry (which encloses the top-level section titles from the page) is inserted before the existing children of the nav entry, effectively becoming a sibling.

|`discrete`
|`discrete`, `fuse`, or `enclose`

|`asciidoc.attributes`
|AsciiDoc document attributes that are applied to each compiled AsciiDoc document when exporting to PDF.
These attributes supplement attributes defined in the playbook, component version descriptors, and pages.
The `pdf-theme` attribute can be used to specify the theme for Asciidoctor PDF.
The `toc` attribute can be used to enable the table of contents in the PDF.
See {url-antora-docs}/playbook/asciidoc-attributes/[Assign Attributes to a Site^] for usage examples and precedence rules.
|`doctype: book`
|Map of built-in and custom AsciiDoc attributes for PDF generation.

|`build.command`
|The command to run to convert an AsciiDoc document to PDF.
|`asciidoctor-pdf`
|A shell command that runs in the context of the environment in which Antora was launched.
If you're using Bundler to manage the Asciidoctor PDF gem, you'll need to set this to `bundle exec asciidoctor-pdf`.
Use the `-r` option to require additional libraries.

|`build.dir`
|The transient directory Assembler uses to prepare and build the PDFs.
|`./build/assembler`
|A directory path.
Uses the same path resolution rules as the output dir in the Antora playbook.

|`build.keep_aggregate_source`
|Whether to keep the aggregate AsciiDoc source files and referenced images under the build dir.
|`false`
|Boolean

|`build.process_limit`
|The number of concurrent processes that Antora should not exceed when converting the compiled AsciiDoc documents to PDFs.
|_auto_ (half the number of CPUs)
|Number

|`build.publish`
|Whether to additionally publish the PDF files to the output destination of the site alongside the HTML files.
|`true`
|Boolean
|===

== Copyright and License

Copyright (C) 2022-present by OpenDevise Inc. and the individual contributors of this project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.
