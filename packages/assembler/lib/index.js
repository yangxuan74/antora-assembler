'use strict'

const assembleContent = require('./assemble-content')
const runCommand = require('./util/run-command')

module.exports = { assembleContent, runCommand }
