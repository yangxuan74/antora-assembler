= The Component
v1.0
:doctype: book
:page-component-name: the-component
:page-component-version: 1.0
:page-version: {page-component-version}
:page-component-display-version: 1.0
:page-component-title: The Component

:docname: the-page
:page-module: ROOT
:page-relative-src-path: the-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#the-page:::]
== The Page Title
:example-caption: Ex

contents

:example-caption: Example

:docname: the-other-page
:page-module: ROOT
:page-relative-src-path: the-other-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#the-other-page:::]
== The Other Page Title

contents
