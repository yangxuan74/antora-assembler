= Antora PDF Extension

The Antora PDF Extension is the official extension for producing PDFs of an Antora site.
Specifically, it generates and publishes PDFs of aggregate pages in the site.

This extension is based on Antora Assembler.
It calls Assembler to construct aggregate AsciiDoc documents from pages based on the navigation tree per component version.
It then iterates over those aggregate documents and converts them to PDF using Asciidoctor PDF or the specified command.
Finally, it pushes those PDFs into the site catalog so they are published as attachments alongside the other files in the site.

== Usage

Once this package is installed in the playbook project, register it in the Antora playbook as follows:

.antora-playbook.yml
[,yaml]
----
antora:
  extensions:
  - '@antora/pdf-extension'
# ...
----

You can configure the behavior of the extension using the optional _antora-assembler.yml_ file.

== Copyright and License

Copyright (C) 2022-present by OpenDevise Inc. and the individual contributors of this project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
